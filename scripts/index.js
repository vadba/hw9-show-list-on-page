// 1. Опишіть, як можна створити новий HTML тег на сторінці.

//      Новий HTML тег на сторінці можна створити за допомогою методу createElement() обєкта Document. Також за допомогою методу createTextNode(), що створює новий текстовий вузол із заданим текстом. insertAdjacentHTML() - додає будь-який HTML на сторінку, разом із новими елементами.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

//      Перший параметр вказує, куди по відношенню до elementa робити вставку. Значення має бути одним із наступних:

//    `beforebegin' - вставити html безпосередньо перед elementa,
//    `afterbegin' - вставити html на початок elementa,
//    `beforeend' - вставити html в кінець elementa,
//    `afterend' - вставити html безпосередньо після elementa.

// 3. Як можна видалити елемент зі сторінки?

//      Для видалення вузла є метод node.remove().

const arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

const arrList = (arr, domElem = document.body) => {
    const domElement = document.querySelector(domElem);
    const ulContent = document.createElement('ul');

    const listAll = arr.map(item => {
        const list = document.createElement('li');

        list.classList.add('list');
        list.textContent = item.toUpperCase();

        return list;
    });
    ulContent.append(...listAll);
    const domElemUl = domElement.append(ulContent);

    return domElemUl;
};

arrList(arr, '.list');

const arr2 = ['Kharkiv', 'Kiev', ['Borispol', 'Irpin'], 'Odessa', 'Lviv', 'Dnieper'];

const arrListRecurs = (arr, domElem = document.body) => {
    let domElement;
    if (domElem !== document.body && !(domElem instanceof HTMLElement)) {
        domElement = document.querySelector(domElem);
    } else {
        domElement = domElem;
    }
    const ulContent = document.createElement('ul');
    domElement?.append(ulContent);

    arr.forEach(item => {
        const list = document.createElement('li');

        list.classList.add('list');
        if (Array.isArray(item)) {
            arrListRecurs(item, list);
        } else {
            list.textContent = item.toUpperCase();
        }

        ulContent.append(list);
    });
};

arrListRecurs(arr2, '.list2');

const divTime = document.querySelector('.time');
const container = document.querySelector('.container');

let timeToNY;
const start = Date.now();
const toTime = Date.now() + 3000;
let difference = toTime - start;

const showTime = () => {
    setTimeout(() => {
        const second = Math.floor((difference / 1000) % 60);
        difference -= 1000;

        timeToNY = `Секунд: ${second}`;
        divTime.innerText = timeToNY;
        if (difference >= -1000) {
            showTime();
        } else {
            divTime.innerText = '';
            divTime.remove();
            container.remove();
            return;
        }
    }, 1000);
};

showTime();
